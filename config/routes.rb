Rails.application.routes.draw do
  put 'person/upsert'
  get 'person/send_line'

  put 'building/upsert'
  get 'building/send_line'

  get 'index/index'

  root 'index#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
