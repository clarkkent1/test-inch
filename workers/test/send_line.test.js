const sinon = require('sinon');
const { expect } = require('chai');
const amqp = require('amqplib');
const request = require('request-promise');
const { sendLine } = require('../send_line');

const sandbox = sinon.sandbox.create();

describe('Send CSV line job', () => {
  let connectionMock;
  let channelMock;

  beforeEach(() => {
    sandbox.stub(amqp);
    channelMock = {
      assertQueue: () => (Promise.resolve(true)),
      prefetch: () => (true),
      consume: () => {
        return Promise.resolve(JSON.stringify({ content: { type: 'test', data: 'data' }}))
      },

    }
    connectionMock = {
      createChannel: () => (channelMock)
    };
  });
  //
  afterEach(() => sandbox.restore());
  //
  describe('#init', () => {
    it('should connect to RMQ', () => {
      amqp.connect.resolves(connectionMock);
      const res = sendLine()
      res.then((msg) => {
        const content = JSON.parse(msg);
        expect(content.data).to.equal('data');
      });
    });

    it('should fail to connect to RMQ', () => {
      connectionMock.createChannel = () => (Promise.reject());
      amqp.connect.resolves(connectionMock);
      const res = sendLine()
      res.catch((msg) => {
        expect(msg).to.not.have.property('content');
      });
    });
  });


});
