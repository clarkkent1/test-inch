const chai = require('chai');
const sinonChai = require('sinon-chai');
const dirtyChai = require('dirty-chai');
const chaiAsPromised = require('chai-as-promised');

require('sinon-as-promised');

chai.use(sinonChai);
chai.use(dirtyChai);
chai.use(chaiAsPromised);
