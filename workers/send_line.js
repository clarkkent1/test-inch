#!/usr/bin/env node
'use strict';
const open = require('amqplib');
const request = require('request-promise');
const sendLine = () => {
  const queue = 'task_csv';
  return open.connect('amqp://amqp')
  .then((conn) => {
    return conn.createChannel();
  })
  .then((ch) => {
    ch.prefetch(1);
    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);
    return ch.assertQueue(queue, {durable: true}).then((ok) => {
      return ch.consume(queue, (msg) => {
        const content = JSON.parse(msg.content.toString());
        console.log(" [x] Received %s", content);
        request({
          method: 'PUT',
          uri: `http://web:3000/${content.type}/upsert`,
          body: {
              data: content
          },
          json: true // Automatically stringifies the body to JSON
        }).then((parsedBody) => {
          ch.ack(msg);
          console.log('Message has been sent ')
          console.log(parsedBody);
        }).catch((err) => {
          ch.ack(msg);
          // console.error(err);
        });
        console.log(" [x] Done");
      }, {noAck: false});
    })
  });
};

module.exports = { sendLine }
