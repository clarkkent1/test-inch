#!/usr/bin/env bash

if !(sha1sum -c package.json.sha1) >/dev/null 2>&1; then
  set -e
  npm install
  sha1sum package.json > package.json.sha1
  set +e
fi

function wait_for_it {
  until (echo > /dev/tcp/$1/$2) >/dev/null 2>&1; do
    # >&2 echo "$1:$2 is unavailable - sleeping"
    sleep .5
  done
}

wait_for_it amqp 5672

PATH=$PATH:./node_modules/.bin/

pipe=`mktemp -u`
mkfifo $pipe
bunyan < $pipe &
exec npm start > $pipe
