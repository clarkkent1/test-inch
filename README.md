Launch
=======
In order to have the best compatible dev environment, I prepare a Rails/Rmq/Node/Postgres Docker.
If you want to launch the complete environment, you need Docker
Database is a container, you just need to execute rake db:create and migrate

***Be sure that the tmp folder is empty***

````shell
cd /path/to/project/test-inch
docker-compose up
docker-compose run web rake db:create -> be sure that db is ready !
docker-compose run web rake db:migrate
````

If you want to stop and remove all container
````shell
docker-compose down
````

Test
========
Test Ruby On Rails
````shell
docker-compose run web rails test
````

Test Node
````shell
docker-compose run worker npm test
````

Conclusion
========
I'm sure you will not understand why I used Rails and Node for this app.
Of course we can discuss on this

I had doubt on integrity constraints and updating system.

About unit test, I had some trouble to add Rspec on Rails 5, so I used the default tool, the coverage is not enough with it.