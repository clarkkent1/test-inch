require 'csv'
class BuildingController < ApplicationController
  include ServicesConcern
  def upsert
    Rails.logger.debug 'fdsfsdfdsdf'
    if request.put?
      building = Building.new
      building.upsert params[:data]
      respond_to do |format|
        format.json { render :json => {:success => true}}
        format.html { render :json => {:success => true}}
      end
    end
  end

  def send_line
    q = connect_to_rmq
    begin
      # Best solution to parse csv file
      # foreach i using IO ruby file reading
      CSV.foreach Rails.root.join("public", "building.csv") do |row|
        if row[0] === "reference"
          next
        end
        q.publish({
          :type => "building",
          :ref => row[0],
          :address => row[1],
          :zip_code => row[2],
          :city => row[3],
          :country => row[4],
          :manager => row[5]
        }.to_json, :persistent => true)
      end
      @message = "All line hass been sent to RMQ, all will be inserted in your database in a minute"
    rescue Exception => e
      raise "Sorry an error has occured when trying to parse your CSV file : #{e.message}"
    end
  end
end
