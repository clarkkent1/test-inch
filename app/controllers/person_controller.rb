require 'csv'
class PersonController < ApplicationController
  include ServicesConcern
  def upsert
    if request.put?
      person = Person.new
      person.upsert params[:data]
    end
    respond_to do |format|
      format.json { render :json => {:success => true}}
      format.html { render :json => {:success => true}}
    end
  end

  def send_line
    q = connect_to_rmq
    begin
      # Best solution to parse csv file
      # foreach i using IO ruby file reading
      CSV.foreach Rails.root.join("public", "people.csv") do |row|
        if row[0] === "reference"
          next
        end
        q.publish({
          :type => "person",
          :ref => row[0],
          :firstname => row[1],
          :lastname => row[2],
          :home_phone_number => row[3],
          :mobile_phone_number => row[4],
          :email => row[5],
          :address => row[6]
        }.to_json, :persistent => true)
      end
      @message = "All line has been sent to RMQ, all will be inserted in your database in a minute"
    rescue Exception => e
      raise "Sorry an error has occured when trying to parse your CSV file : #{e.message}"
    end
  end
end
