require 'bunny'
module ServicesConcern
  extend ActiveSupport::Concern

  def connect_to_rmq()
    begin
      conn = Bunny.new :host => "amqp"
      conn.start
      ch = conn.create_channel
      q = ch.queue("task_csv", :durable => true)
      return q
    rescue Exception => e
      raise "Sorry an error has occured when trying to connect to RMQ : #{e.message}"
    end
  end
end
