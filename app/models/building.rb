class Building < ApplicationRecord

  validates :reference, presence: true
  validates :address, presence: true
  validates :zip_code, presence: true
  validates :city, presence: true
  validates :country, presence: true
  validates :manager_name, presence: true

  self.table_name = 'buildings'
  def upsert(row = nil)
    unless row === nil
      reference = ActiveRecord::Base::sanitize(row[:ref])
      address = ActiveRecord::Base::sanitize(row[:address])
      zip_code = ActiveRecord::Base::sanitize(row[:zip_code])
      city = ActiveRecord::Base::sanitize(row[:city])
      country = ActiveRecord::Base::sanitize(row[:country])
      manager_name = ActiveRecord::Base::sanitize(row[:manager])
      self.reference = row[:ref]
      self.address = row[:address]
      self.zip_code = row[:zip_code]
      self.city = row[:city]
      self.country = row[:country]
      self.manager_name = row[:manager]
      unless valid?
        return false
      end
      sql = "insert into buildings
        (reference, address, zip_code, city, country, manager_name, created_at, updated_at)
        values (#{reference}, #{address}, #{zip_code}, #{city}, #{country}, #{manager_name}, NOW(), NOW())
        on conflict (reference)
        do update set (manager_name) = (#{manager_name})
        where buildings.address = #{address};"
      begin
        ActiveRecord::Base.connection.raw_connection.prepare("insert-building#{Random.new_seed}", sql)
        result = ActiveRecord::Base.connection.execute(sql)
        Rails.logger.debug result
      rescue Exception => e
        raise e.message
      end
    end
  end
end
