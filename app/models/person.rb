class Person < ApplicationRecord
  validates :reference, presence: true
  validates :home_phone_number, presence: true
  validates :mobile_phone_number, presence: true
  validates :email, presence: true
  validates :address, presence: true

  self.table_name = 'persons'
  def upsert(row = nil)
    unless row === nil
      reference = ActiveRecord::Base::sanitize(row[:ref])
      firstname = ActiveRecord::Base::sanitize(row[:firstname])
      lastname = ActiveRecord::Base::sanitize(row[:lastname])
      home_phone_number = ActiveRecord::Base::sanitize(row[:home_phone_number])
      mobile_phone_number = ActiveRecord::Base::sanitize(row[:mobile_phone_number])
      email = ActiveRecord::Base::sanitize(row[:email])
      address = ActiveRecord::Base::sanitize(row[:address])
      self.reference = row[:ref]
      self.firstname = row[:firstname]
      self.lastname = row[:lastname]
      self.home_phone_number = row[:home_phone_number]
      self.mobile_phone_number = row[:mobile_phone_number]
      self.email = row[:email]
      self.address = row[:address]
      unless valid?
        return false
      end
      sql = "insert into persons
        (reference, firstname, lastname, home_phone_number, mobile_phone_number, email, address, created_at, updated_at)
        values (#{reference}, #{firstname}, #{lastname}, #{home_phone_number}, #{mobile_phone_number}, #{email}, #{address}, NOW(), NOW())
        on conflict (home_phone_number)
        do update set (reference, email, home_phone_number, mobile_phone_number, address) = (#{reference}, #{email}, #{home_phone_number}, #{mobile_phone_number}, #{address})
        where persons.home_phone_number = #{home_phone_number};"
      begin
        ActiveRecord::Base.connection.raw_connection.prepare("insert-person#{Random.new_seed}", sql)
        result = ActiveRecord::Base.connection.execute(sql)
      rescue Exception => e
        raise e.message
      end
    end
  end
end
