class CreateBuildings < ActiveRecord::Migration[5.0]
  def change
    create_table :buildings do |t|
      t.string :reference
      t.string :address
      t.string :zip_code
      t.string :city
      t.string :country
      t.string :manager_name

      t.timestamps
    end
    add_index :buildings, :reference, :unique => true
    add_index :buildings, :address, :unique => true
    add_index :buildings, :manager_name
  end
end
