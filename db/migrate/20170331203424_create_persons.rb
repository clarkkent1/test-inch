class CreatePersons < ActiveRecord::Migration[5.0]
  def change
    create_table :persons do |t|
      t.string :reference
      t.string :email
      t.string :home_phone_number
      t.string :mobile_phone_number
      t.string :firstname
      t.string :lastname
      t.string :address

      t.timestamps
    end
    add_index :persons, :reference, :unique => true
    add_index :persons, :email, :unique => true
    add_index :persons, :home_phone_number, :unique => true
    add_index :persons, :mobile_phone_number, :unique => true
  end
end
