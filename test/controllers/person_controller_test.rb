require 'test_helper'

class PersonControllerTest < ActionDispatch::IntegrationTest
  test "should get send_line" do
    get url_for(:controller => :person, :action => :send_line)
    assert_response :success
  end

  test "should upsert into person table" do
    res = put url_for(:controller => :person, :action => :upsert),
    params: { data: {
      :type => "person",
      :ref => "1",
      :firstname => "fdfsdf",
      :lastname => "fdfsdfdsfsd",
      :home_phone_number => "fdsfsdfdsfd",
      :mobile_phone_number => "ffvxvcxvxc",
      :email => "vvcxxcvxc",
      :address => "vvcxxcvxc"
    } }
    assert_response :success
  end

end
