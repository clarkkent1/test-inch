require 'test_helper'

class BuildingControllerTest < ActionDispatch::IntegrationTest
  test "should get send_line" do
    get url_for(:controller => :building, :action => :send_line)
    assert_response :success
  end

  test "should upsert into building table" do
    res = put url_for(:controller => :building, :action => :upsert),
    params: { data: {
      :type => "building",
      :ref => "1",
      :address => "fdfsdf",
      :zip_code => "fdfsdfdsfsd",
      :city => "fdsfsdfdsfd",
      :country => "ffvxvcxvxc",
      :manager => "vvcxxcvxc"
    } }
    assert_response :success
  end

end
