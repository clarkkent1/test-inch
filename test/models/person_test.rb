require 'test_helper'

class PersonTest < ActiveSupport::TestCase
  test "upsert record" do
    row = {
      :ref => "1",
      :firstname => "fdfsdf",
      :lastname => "fdfsdfdsfsd",
      :home_phone_number => "fdsfsdfdsfd",
      :mobile_phone_number => "ffvxvcxvxc",
      :email => "vvcxxcvxc",
      :address => "vvcxxcvxc"
    }
    person = Person.new
    assert person.upsert(row)
  end

  test "not upsert record because some data are missing" do
    row = {
      :ref => "1",
      :firstname => "fdfsdf",
      :lastname => "fdfsdfdsfsd",
      :address => "vvcxxcvxc"
    }
    person = Person.new
    assert_not person.upsert(row)
  end
end
