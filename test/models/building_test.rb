require 'test_helper'

class BuildingTest < ActiveSupport::TestCase
  test "upsert record" do
    row = {
      :ref => "1",
      :address => "fdfsdf",
      :zip_code => "fdfsdfdsfsd",
      :city => "fdsfsdfdsfd",
      :country => "ffvxvcxvxc",
      :manager => "vvcxxcvxc"
    }
    building = Building.new
    assert building.upsert(row)
  end

  test "not upsert record because some data are missing" do
    row = {
      :address => "fdfsdf",
      :city => "fdsfsdfdsfd",
      :country => "ffvxvcxvxc",
      :manager => "vvcxxcvxc"
    }
    building = Building.new
    assert_not building.upsert(row)
  end
end
